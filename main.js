// Exercise 1
function calWage() {
  var wageValue = document.getElementById("wage").value;
  var dayValue = document.getElementById("workingDay").value;
  var yourWage = wageValue * dayValue;
  document.getElementById("result1").innerHTML = new Intl.NumberFormat(
    "vn-VN",
    {
      style: "currency",
      currency: "VND",
    }
  ).format(yourWage);
}

// Exercise 2
function calAverageNum() {
  var number1Value = document.getElementById("number1").value;
  var number2Value = document.getElementById("number2").value;
  var number3Value = document.getElementById("number3").value;
  var number4Value = document.getElementById("number4").value;
  var number5Value = document.getElementById("number5").value;
  var averageNumber =
    (+number1Value +
      +number2Value +
      +number3Value +
      +number4Value +
      +number5Value) /
    5;
  document.getElementById("result2").innerHTML = averageNumber;
}

// Exercise 3
function exchangeMoney() {
  var usdMoneyValue = document.getElementById("usdmoney").value;
  var vndMoney = usdMoneyValue * 23500;
  document.getElementById("result3").innerHTML = new Intl.NumberFormat(
    "vn-VN",
    {
      style: "currency",
      currency: "VND",
    }
  ).format(vndMoney);
}

// Exercise 4
function calAreaPeri() {
  var longsValue = document.getElementById("longs").value;
  var widthValue = document.getElementById("width").value;
  var areaValue = longsValue * widthValue;
  var periValue = (+longsValue + +widthValue) * 2;
  document.getElementById("result4_1").innerHTML = areaValue;
  document.getElementById("result4_2").innerHTML = periValue;
}

// Exercise 5
function calSum() {
  var numberValue = document.getElementById("number").value;
  var tensValue = Math.floor(numberValue / 10);
  var unitValue = numberValue % 10;
  var sumValue = tensValue + unitValue;
  document.getElementById("result5").innerHTML = sumValue;
}
